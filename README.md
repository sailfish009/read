# read
[![Crates.io](https://img.shields.io/crates/v/read.svg)](https://crates.io/crates/read)
[![License](https://img.shields.io/crates/l/read.svg)](https://bitbucket.org/sailfish009/read)
[![Rust](https://www.rust-lang.org/logos/rust-logo-16x16-blk.png)](https://www.rust-lang.org/en-US/other-installers.html)

win32 editor written in rust

released under zlib license

![Scheme](bin/read.png)
