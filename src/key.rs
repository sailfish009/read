//========================================================================
// read - simple win32 text editor written in rust
//------------------------------------------------------------------------
// Copyright (c) 2019 Ji Wong Park <sailfish009@gmail.com>
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would
//    be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and must not
//    be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source
//    distribution.
//
//========================================================================

fn key_up(w :HWND){let y = getposy();if y == 0 {return;} hidecaret(w);setposmy(1); showcaret(w);}
fn key_down(w :HWND){hidecaret(w);setpospy(1);showcaret(w);}
fn key_left(w :HWND)
{
  if getposx() == 0 {return;}
  *END.lock().unwrap() = 0;
  setposmx(1);
  let index = getindex();
  match index{None => {}, _ => {hidecaret(w);setposx(getx(index.unwrap()));showcaret(w);}}
}
fn key_right(w :HWND, f :HFONT)
{
  let index = getindex();
  match index
  {
    None => (),
    _ =>
    {
      let state = getstate();
      let i = index.unwrap();
      if NEWLINE == getc(i) {return;}
      let end = {*END.lock().unwrap()};
      if (getlength() - 1) == i
      {
        if end != 1
        {
          *END.lock().unwrap() = 1;
          hidecaret(w); setposx(getx(i)+1); showcaret(w);
          if state == 0x88 {drawtext(w, f, &CH{x:getx(i),y:gety(i),c:getc(i)}, 1, 1);}
        }
        return;
      }
      hidecaret(w); setposx(getx(i+1)); showcaret(w);
      if state == 0x88 {drawtext(w, f, &CH{x:getx(i),y:gety(i),c:getc(i)}, 1, 1);}
    },
  }
}
fn wheel_up(w :HWND, f :HFONT)
{
  let delta = {*DEY.lock().unwrap()} as isize;
  if delta == 0 {return};
  clearscreen(w);
  *DEY.lock().unwrap() += 2;
  let delta = {*DEY.lock().unwrap()} as isize;
  let vec = {TEXT.lock().unwrap()};
  vec.iter().for_each(|ref x| match x.c {NEWLINE => {setposx(0);setpospy(1);}, NOTLINE => {}, _ =>{drawtext(w, f, x, 2, delta);}} )
}
fn wheel_down(w :HWND, f :HFONT)
{
  clearscreen(w);
  *DEY.lock().unwrap() -= 2;
  let delta = {*DEY.lock().unwrap()} as isize;
  let vec = {TEXT.lock().unwrap()};
  vec.iter().for_each(|ref x| match x.c {NEWLINE => {setposx(0);setpospy(1);}, NOTLINE => {}, _ =>{drawtext(w, f, x, 2, delta);}} )
}
fn ctrl(w :HWND, p :WPARAM, wm :&mut WM)
{
  println!("p: 0x{0:02x}", p as u8);
  let f = wm.font; 
  match p 
  {
    0x08 =>  {let sw = wm.sw[0]; sidepanel(w, sw, f)},    // ctrl + h 
    0x0a =>  {let sw = wm.sw[1]; sidepanel(w, sw, f)},    // ctrl + j 
    0x0b =>  {let sw = wm.sw[2]; sidepanel(w, sw, f)},    // ctrl + k 
    0x0c =>  {let sw = wm.sw[3]; sidepanel(w, sw, f)},    // ctrl + l 
    0x0F =>                                               // ctrl + o 
    {
      let length = getlength();
      println!("debug, length: {0}", length);
      let vec = {TEXT.lock().unwrap()};
      for val in vec.iter() {print!("[{0:02x},{1},{2}]", val.c as u8, val.x, val.y);} println!{""};
    },
    0x18 =>                                               // ctrl + x 
    {
      let path = String::from("./test.txt");
      file(w, f, path, 0); 
    },
    // debug buffer
    0x13 =>                                               // ctrl + s
    {
      let rbuffer = {RBUFFER.lock().unwrap()};
      let len = rbuffer.len(); 
      println!("debug, length: {0}", len);
      // for val in rbuffer {print!("[{0}]", val as u8);} println!{""};
      println!("[{0}]", rbuffer);
    },
    0x02 =>                                               // ctrl + b 
    {
      clear(); clearscreen(w); hidecaret(w); setpos(0,0); showcaret(w);
    },
    0x03 => copy(w, f),                                   // ctrl + c
    0x16 => paste(w, f),                                  // ctrl + v
    0x17 =>                                               // ctrl + w
    {
      let path = String::from("./help.txt");
      file(w, f, path, 0); 
    },
    _ => (),
  }
}
fn key_0x24()                                             // $: move key to x end
{
  let index = getindex();
  match index
  {
    None => {},
    _ =>
    {
      let index = index.unwrap();
      let length = getlength();
      let y = getposy();
      let w = getw() as i32;
      let h = geth();
      for i in index..length
      {
        if getc(i) == NEWLINE
        {
          let x = getx(i);
          setcaretpos(x*w, y*h);
          setposx(x);
          break;
        }
        else if i == (length-1)
        {
          *END.lock().unwrap() = 1;
          let x = getx(i)+1;
          setcaretpos(x*w, y*h);
          setposx(x);
        }
      }
    },
  }
}
fn key_0x30()                                             // 0: move key to x 0
{
  *END.lock().unwrap() = 0;
  let y = getposy();
  let ch_h = {*CHY.lock().unwrap()};
  setcaretpos(0, y*ch_h);
  setposx(0);
}
fn key_0x44(w :HWND)                                      // key D
{
  hidecaret(w);
  showcaret(w);
}
fn key_0x47(w :HWND)                                      // key G
{
  hidecaret(w); let p = getlastpos(); setpos(p.x, p.y); showcaret(w);
}
fn key_0x64(w :HWND, f :HFONT)                            // key dd
{
  let key = {*KEY.lock().unwrap()};
  match key
  {
    0 => { *KEY.lock().unwrap() = 0x164; },
    // d d
    0x164 =>
    {
      *KEY.lock().unwrap() = 0;
      hidecaret(w);
      let length = getlength();
      match length 
      { 
        0 => (),
        _ =>
        {
          let start = getprevlastx();
          let end = getlastx();
          clearrange(w, f, start, length-1);
          removeline(start, end+1, 1);
          let length = getlength();
          match length { 0 => (), _ => {drawrange(w, f, start, length-1);}}
        }
      }
      showcaret(w);
    },
    _ => { *KEY.lock().unwrap() = 0; },
  }
}
fn key_0x67(w :HWND)                                      // key gg
{
  let key = {*KEY.lock().unwrap()};
  match key
  {
    0 => {*KEY.lock().unwrap() = 0x167;},
    // g + 1
    0x167 =>
    {
      *KEY.lock().unwrap() = 0;
      hidecaret(w); setpos(0, 0); showcaret(w);
    },
    _ => {*KEY.lock().unwrap() = 0;},
  }
}
fn key_0x6f(w :HWND, f :HFONT, icon :HICON)               // o: change mode 
{
  hidecaret(w); 
  setmode(3); change_state(w, icon); 

  enter(w,f); 
  setposx(0); setpospy(1); 
  enter(w,f);

  showcaret(w);
}
fn key_0x70(w :HWND, f :HFONT)                            // key p
{
  let mut rbuffer = {RBUFFER.lock().unwrap()};
  let lastx = getlastx();
  let len = rbuffer.len(); 
  let length = getlength();
  clearrange(w, f, lastx, length-1);

  let lastx = getlastx();

  let posy = getposy() + 1;
  setpos(0, posy);
  println!("lastx: {0}", lastx);
  for v in 0..len
  {
    insert(lastx+1, CH{x:getposx() + (len - v -1) as i32, y:getposy(), c:rbuffer.pop().unwrap()});
  }

  moveline(lastx+len);
  let length = getlength();
  match length { 0 => (), _ => {drawrange(w, f, lastx, length-1);},}
}
fn key_0x78(w :HWND, f :HFONT)                            // key x
{
  delete(w, f, 0);
}
fn key_0x79(w :HWND, f :HFONT, icon :HICON)               // key y
{
  let state = getstate();
  match state & 0x88
  {
    0x88 => 
    {
      setstate(0xFFFFFF77, 1); change_state(w, icon);
      setmode(0);
      let index = getindex();
      match index { None => (), _ => {setregion(index.unwrap()-1, 1); } }
      copy2(w, f, 1);
    },
    _ => (),
  }
}
fn key_0x1b(w :HWND, f :HFONT, icon :HICON)               // esc
{
  match getmode()
  {
    0 => (),
    _ =>
    {
      setmode(0);
      change_state(w, icon);
      let state = getstate();
      match state & 0x88
      {
        0x88 => 
        {
          setstate(0xFFFFFF77, 1); 
          let index = getindex();
          match index { None => (), _ => {setregion(index.unwrap()-1, 1);} }
          let region = {REGION.lock().unwrap()};
          if region.s != region.e {redraw(w, f, 0, 1, region.s, region.e);}
        },
        _ => (),
      }
    }
  }
}
fn key_0x0d(w :HWND, f :HFONT)                            // enter
{
  enter(w, f); hidecaret(w); setposx(0); setpospy(1); showcaret(w);
}
fn key_input(w :HWND, f :HFONT, p :WPARAM)                // edit
{
  let x = getposx();
  let y = getposy();
  let index = getindex();
  let c = unsafe{std::char::from_u32_unchecked(p as u32)}; 
  match index
  {
    None => 
    {
      // println!("end text");
      let ch = CH{x:x,y:y,c:c};
      hidecaret(w); drawtext(w,f,&ch,0,0); save(ch); setpospx(1); showcaret(w);
    },
    _ => 
    {
      // println!("middle text");
      let index = index.unwrap();
      let length = getlength();
      if index < length
      {
        let mut line_end = length + 1;
        hidecaret(w);
        for i in index..length
        {
          match NEWLINE == getc(i)
          {
            true => {line_end = i+1;setx(i, getx(i)+1);break;},
            _ => {clearch(w, i, 0); setx(i, getx(i)+1);},
          }
        }
        let ch = CH{x:x as i32,y:y as i32,c:c};
        insert(index, ch);
        for i in index..line_end
        {
          drawtext(w, f, &CH{x:getx(i) as i32,y:gety(i) as i32,c:getc(i)}, 1, 0);  
        }
        setpospx(1);
        showcaret(w);
      }
      else
      {
        println!("index >= length");
      }
    },
  }
}
fn edit(w :HWND, p :WPARAM, wm :&WM)
{
  unsafe
  {
    // println!("p: 0x{0:02x}", p as u8);
    let f = wm.font; let icon = wm.icon[0]; let icon2 = wm.icon[1]; let icon3 = wm.icon[2]; 
    match getmode()
    {
      // save mode
      0 =>
      {
        hidecaret(w);
        match p
        {
          0x69 => {setmode(2); change_state(w, icon2);},  // i: change mode 
          0x61 => {setmode(1); change_state(w, icon2);},  // a: change mode 
          0x6f => key_0x6f(w,f,icon2),                    // o: change mode 
          0x76 => {setmode(4); change_state(w, icon3);},  // v: change mode 

          0x78 => key_0x78(w,f),                          // x: delete

          0x24 => key_0x24(),                             // $: move key to x end
          0x30 => key_0x30(),                             // 0: move key to x 0
          0x44 => key_0x44(w),                            // key D
          0x47 => key_0x47(w),                            // key G
          0x64 => key_0x64(w,f),                          // key dd
          0x67 => key_0x67(w),                            // key gg
          0x70 => key_0x70(w, f),                         // key p
          // 0x79 => key_0x79(w, f),                      // key y
          0x7A => println!("0x7A"),                       // key zz

          // h, j, k, l
          0x68 => key_left(w), 0x6A => key_down(w), 0x6B => key_up(w), 0x6C => key_right(w, f),
          _ => (),
        }
        ShowCaret(w);
        return;
      },
      // visual mode
      4 =>
      {
        match getstate() & 0x88
        {
          0x88 => (), 
          _ => // println!("enter visual mode");
          {
            setstate(0x88, 0); 
            let index = getindex();
            match index { None => (), _ => {setregion(index.unwrap(), 0); } }
          }
        }
        
        hidecaret(w);
        match p
        {
          0x79 => key_0x79(w, f, icon),                   // key y
          0x1B => key_0x1b(w, f, icon),                   // esc

          // h, j, k, l
          0x68 => key_left(w), 0x6A => key_down(w), 0x6B => key_up(w), 0x6C => key_right(w, f),
          _ => (),
        }
        ShowCaret(w);
        return;
      },
      // edit mode, bypass
      _ => (),
    }

    match p 
    {
      0x08 => delete(w, f, 1),                            // backspace
      0x0D => key_0x0d(w, f),                             // enter 
      0x1B => key_0x1b(w, f, icon),                       // esc
      _ => key_input(w, f, p),                            // edit
    }
  }
}
fn sidepanel(w: HWND, sw: HWND, f :HFONT)
{
  unsafe
  {
    match getmode() & 0x10
    {
      0x10 =>
      {
        showcaret(w);
        *MODE.lock().unwrap() -= 0x10;
        ShowWindow(sw, SW_HIDE);
        SetFocus(w);
        redraw(w, f, 0, 0, 0, 0); 
      },
      _ =>
      {
        hidecaret(w);
        *MODE.lock().unwrap() += 0x10;
        ShowWindow(sw, SW_SHOW);
        SetFocus(w);
      },
    }
  }
}

