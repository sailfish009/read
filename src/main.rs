//========================================================================
// read - simple win32 text editor written in rust
//------------------------------------------------------------------------
// Copyright (c) 2019 Ji Wong Park <sailfish009@gmail.com>
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would
//    be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and must not
//    be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source
//    distribution.
//
//========================================================================

// hide console window
// #![windows_subsystem = "windows"]

// const, I/O, TEXT, KEY, GUI, WNDPROC
include!("const.rs"); include!("io.rs"); include!("text.rs"); include!("key.rs"); include!("gui.rs"); include!("proc.rs");

struct SA {s :usize, e :usize}
struct CH {x :LONG,  y :LONG,  c :char}
struct WM <'a> {font :HFONT, icon:&'a[HICON], sw :&'a[HWND]}

lazy_static!
{
  // MODE:  0: save  1(i),2(a): edit
  static ref MODE: Mutex<u8>       = Mutex::new(0);              static ref INDEX: Mutex<u32>      = Mutex::new(0);
  static ref TEXT: Mutex<Vec<CH>>  = Mutex::new(Vec::new());     static ref POS: Mutex<POINT>      = Mutex::new(POINT{x:0, y:0});
  static ref CHX: Mutex<LONG>      = Mutex::new(0);              static ref CHY: Mutex<LONG>       = Mutex::new(0);
  static ref DEY: Mutex<LONG>      = Mutex::new(0);              static ref END: Mutex<u8>         = Mutex::new(1);
  static ref KEY: Mutex<u32>       = Mutex::new(0);              static ref STATE: Mutex<u32>      = Mutex::new(0);
  static ref SCREEN: Mutex<LONG>   = Mutex::new(31);             static ref REGION: Mutex<SA>      = Mutex::new(SA{s:0, e:0});
  static ref BUFFER: Mutex<String> = Mutex::new(String::new());  static ref RBUFFER: Mutex<String> = Mutex::new(String::new());
}

// main loop
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
fn main()
{
  unsafe
  {
    let class_name = wstr("window"); let win_name = wstr("read v0.1.2");
    let c1 = wstr("sub1");let w1 = wstr("win1"); let c2 = wstr("sub2");let w2 = wstr("win2");
    let c3 = wstr("sub3");let w3 = wstr("win3"); let c4 = wstr("sub4");let w4 = wstr("win4");
    let font = font(wstr("Dejavu Sans Mono"), 18);
    let loadicon = |r :u8, g :u8, b :u8| -> HICON {geticon(RGB(r,g,b), 16, 16)};
    let icon : [HICON; 4] = [loadicon(0xC0,0xC0,0xC0), loadicon(0,0,0), loadicon(0x5F,0xBC,0xE4), null()];
    let brush :HBRUSH = CreateSolidBrush(RGB(R_B, G_B, B_B));
    let console_brush :HBRUSH = CreateSolidBrush(RGB(C_R_B, C_G_B, C_B_B));

    let mut cwm = WM{font :font, icon :&icon, sw: &[ptr::null_mut()] };
    let cwm_ptr = &mut cwm as *mut _ as LPVOID;
    let wr : [HWND; 4] =
    [
      window(c1, w1, Some(sw1_proc), icon[0], WS_POPUP|WS_BORDER, -1, -1, W/4, H, 0 as LPVOID),                              // left
      window(c2, w2, Some(sw2_proc), icon[0], WS_POPUP|WS_HSCROLL|WS_VSCROLL|WS_VISIBLE, -1, 2*H/3, W, H/3, cwm_ptr),        // bottom
      window(c3, w3, Some(sw3_proc), icon[0], WS_POPUP|WS_BORDER|WS_VISIBLE, -1, -1, W, 2*H/3, cwm_ptr),                     // top
      // window(c2, w2, Some(sw2_proc), icon[0], WS_POPUP|WS_HSCROLL|WS_VSCROLL|WS_VISIBLE, -1, 2*H/3, W, H/3, 0 as LPVOID), // bottom
      // window(c3, w3, Some(sw3_proc), icon[0], WS_POPUP|WS_BORDER|WS_VISIBLE, -1, -1, W, 2*H/3, 0 as LPVOID),              // top
      window(c4, w4, Some(sw4_proc), icon[0], WS_POPUP|WS_BORDER, 6*W/7, -1, W/7, H, 0 as LPVOID)                            // right
    ];

    let mut wm = WM{font :font, icon :&icon, sw :&wr};
    let wm_ptr = &mut wm as *mut _ as LPVOID;
    let hwnd = window(class_name, win_name, Some(window_proc), icon[0] as HICON, WS_OVERLAPPEDWINDOW | WS_VISIBLE, 0, 0, W, H, wm_ptr);
    // let hwnd = window(class_name, win_name, Some(window_proc), icon[0] as HICON, WS_VISIBLE, 0, 0, W, H, wm_ptr);
    // let hwnd = window(class_name, win_name, Some(window_proc), icon[0] as HICON, WS_OVERLAPPEDWINDOW|WS_HSCROLL|WS_VSCROLL|WS_VISIBLE, 0, 100, W, H, wm_ptr);

    let attr :u32 = 20;
    let mut dark_mode :u32 = 1;
    let dark_mode_ptr =  &mut dark_mode as *mut _ as *mut c_void;
    let sizeof_int :u32 = 4;
    DwmSetWindowAttribute(hwnd, attr, dark_mode_ptr, sizeof_int);


    ShowWindow(hwnd, SW_SHOW);
    for i in 0..4 { SetParent(wr[i], hwnd);}
    SetFocus(hwnd);
    let mut msg = MSG {hwnd : 0 as HWND,message : 0 as UINT, wParam : 0 as WPARAM, lParam : 0 as LPARAM, time : 0 as DWORD, pt : POINT{x:0, y:0},};
    if font != null() {SendMessageW(hwnd, WM_SETFONT, font as WPARAM, 1);}
    let font_size :POINT = getfontwh(hwnd, font);
    *CHX.lock().unwrap() = font_size.x; *CHY.lock().unwrap() = font_size.y;
    CreateCaret(hwnd, 0 as HBITMAP, 1, font_size.y);
    showcaret(hwnd);
    DragAcceptFiles(hwnd, 1);
    // background(hwnd, &brush);

    background(hwnd, &brush);
    background(wr[1], &console_brush);
    MoveWindow(hwnd, SX, SY, W, H, 1);
    InvalidateRect(hwnd, null(), 1);
    loop
    {
      let m = GetMessageW(&mut msg, 0 as HWND, 0, 0);
      match msg.message {WM_QUIT => break, _ => {if m > 0 { TranslateMessage(&mut msg);DispatchMessageW(&mut msg);}}}
    }
    DeleteObject(brush as HGDIOBJ);
  }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
