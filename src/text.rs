//========================================================================
// read - simple win32 text editor written in rust
//------------------------------------------------------------------------
// Copyright (c) 2019 Ji Wong Park <sailfish009@gmail.com>
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would
//    be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and must not
//    be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source
//    distribution.
//
//========================================================================

// TEXT :  static ref TEXT: Mutex<Vec<CH>>  = Mutex::new(Vec::new());

fn clear() {let mut vec = {TEXT.lock().unwrap()}; vec.clear();}
fn remove(i :usize){let mut vec = {TEXT.lock().unwrap()}; vec.remove(i);}
fn insert(i :usize, c :CH){let mut vec = {TEXT.lock().unwrap()}; vec.insert(i, c)}
fn save(c :CH){let mut vec = {TEXT.lock().unwrap()}; vec.push(c);}
fn getlength() -> usize {let vec = {TEXT.lock().unwrap()}; let length = vec.len();length}
fn getx(i :usize) -> LONG {let vec = {TEXT.lock().unwrap()}; let x = vec[i].x; x}
fn gety(i :usize) -> LONG {let vec = {TEXT.lock().unwrap()}; let y = vec[i].y; y}
fn getc(i :usize) -> char {let vec = {TEXT.lock().unwrap()}; let c = vec[i].c; c}
fn setx(i :usize, x :LONG){let mut vec = {TEXT.lock().unwrap()}; vec[i].x = x;}
// fn sety(i :usize, y :LONG){let mut vec = {TEXT.lock().unwrap()}; vec[i].y = y;}
fn getprevlastx() -> usize
{
  let y = {POS.lock().unwrap().y};
  if y == 0 {return 0}
  let vec = {TEXT.lock().unwrap()};
  let index = vec.iter().position(|ref e| ((e.c == NEWLINE) && (e.y == (y-1))));
  match index { None => {return 0}, _ => {index.unwrap() + 1}}
}
fn getlastx() -> usize
{
  let y = {POS.lock().unwrap().y};
  let vec = {TEXT.lock().unwrap()};
  let index = vec.iter().position(|ref e| ((e.c == NEWLINE) && (e.y == y)));
  match index {None => {vec.len()-1}, _ => {index.unwrap()}}
}
fn getlastpos() -> POINT
{
  let vec = {TEXT.lock().unwrap()}; 
  let len = vec.len();
  match len
  {
    0 => {POINT{x:0, y:0}},
    _ => 
    {
      match vec[len-1].c
      {
        NEWLINE => POINT{x:0, y:vec[len-1].y+1},
        _ => POINT{x:vec[len-1].x+1, y:vec[len-1].y},
      }
    },
  }
}
fn getindex() -> Option<usize>
{
  let x = {POS.lock().unwrap().x};
  let y = {POS.lock().unwrap().y};
  let vec = {TEXT.lock().unwrap()};
  vec.iter().position(|ref e| ((e.x == x) && (e.y == y)))
}
fn getchar(x :i32, y :i32) -> Option<char>
{
  let vec = {TEXT.lock().unwrap()};
  let index = vec.iter().position(|ref e| ((e.x == x) && (e.y == y)));
  match index {None => {None}, _ => {Some(vec[index.unwrap()].c)}}
}
fn strlength(buffer :*mut u8) -> usize
{
  unsafe {let mut i : isize = 0; while *buffer.offset(i) != 0 {i+=1;} i as usize}
}
// fn setrbuffer(s :usize, e :usize, rstring :&mut String)
// {
//   let mut vec = {TEXT.lock().unwrap()};
//   for i in s..=e {rstring.push(vec.pop().unwrap().c)}
// }
fn setbuffer(s :usize, e :usize, string :&mut String)
{
  let vec = {TEXT.lock().unwrap()};
  vec[s..=e].iter().for_each(|ref x| string.push(x.c));
}
fn removeline(start :usize, end :usize, mode :u8)
{
  // end must be (indexof(NEWLINE) + 1)
  let mut vec = {TEXT.lock().unwrap()}; 
  match mode
  {
    0 => for _i in start..end{vec.remove(start);},
    _ => 
    {
      let mut rbuffer = {RBUFFER.lock().unwrap()};
      rbuffer.clear();
      let mut region = {REGION.lock().unwrap()};
      region.s = start; region.e = end;
      for _i in start..end{let ch = vec.remove(start); rbuffer.push(ch.c);}
    }
  }
  for i in start..vec.len() {vec[i].y -= 1;}
}
fn moveline(start :usize)
{
  let mut vec = {TEXT.lock().unwrap()}; 
  for i in start..vec.len() {vec[i].y += 1;}
}
fn end(index :usize) -> bool
{
  let vec = {TEXT.lock().unwrap()}; 
  let len = vec.len() - 1;
  index == len
}
