// utils.rs : public domain license
// ported c++ version CreateSolidColorIcon() to rust's
// https://stackoverflow.com/questions/16472538/changing-taskbar-icon-programatically-win32-c/16473172

extern crate winapi; 

use winapi::um::{winuser as user, wingdi as gdi};
use winapi::shared::windef::{HWND, COLORREF, HBRUSH, HPEN, HICON, HFONT, HGDIOBJ, HBITMAP, HDC, POINT};
use winapi::shared::minwindef::{INT, UINT};
use user::{GetDC, ReleaseDC};
use user::{CreateIconIndirect, ICONINFO};
use gdi::{SelectObject, DeleteObject, TEXTMETRICW, GetCharWidth32W, GetTextMetricsW};
use gdi::{CreateSolidBrush, CreateCompatibleDC, CreateCompatibleBitmap};
use gdi::{DeleteDC, Rectangle, CreatePen}; 
use ptr::null_mut as null;

pub fn geticon(icon_color :COLORREF, width :i32, height :i32) -> HICON
{
  unsafe
  {
    // Obtain a handle to the screen device context.
    let hdc_screen = GetDC(null()) as HDC;

    // Create a memory device context, which we will draw into.
    let hdc_mem = CreateCompatibleDC(hdc_screen) as HDC;

    // Create the bitmap, and select it into the device context for drawing.
    let hbmp = CreateCompatibleBitmap(hdc_screen, width, height) as HBITMAP;    
    let hbmp_old = SelectObject(hdc_mem, hbmp as HGDIOBJ) as HBITMAP;

    // Draw your icon.
    // 
    // For this simple example, we're just drawing a solid color rectangle
    // in the specified color with the specified dimensions.
    let hpen = CreatePen(gdi::PS_SOLID as i32, 1, icon_color) as HPEN;
    let hpen_old = SelectObject(hdc_mem, hpen as HGDIOBJ) as HPEN;
    let hbrush = CreateSolidBrush(icon_color) as HBRUSH;
    let hbrush_old = SelectObject(hdc_mem, hbrush as HGDIOBJ) as HBRUSH;
    Rectangle(hdc_mem, 0, 0, width, height);
    SelectObject(hdc_mem, hbrush_old as HGDIOBJ);
    SelectObject(hdc_mem, hpen_old as HGDIOBJ);
    DeleteObject(hbrush as HGDIOBJ);
    DeleteObject(hpen as HGDIOBJ);

    // Create an icon from the bitmap.
    // 
    // Icons require masks to indicate transparent and opaque areas. Since this
    // simple example has no transparent areas, we use a fully opaque mask.
    let hbmp_mask = CreateCompatibleBitmap(hdc_screen, width, height) as HBITMAP;
    let mut ii  = ICONINFO{fIcon :1, xHotspot :0, yHotspot :0, hbmMask :hbmp_mask, hbmColor :hbmp};
    let h_icon = CreateIconIndirect(&mut ii as *mut user::ICONINFO) as HICON;
    DeleteObject(hbmp_mask as HGDIOBJ);

    // Clean-up.
    SelectObject(hdc_mem, hbmp_old as HGDIOBJ);
    DeleteObject(hbmp as HGDIOBJ);
    DeleteDC(hdc_mem);
    ReleaseDC(null(), hdc_screen);

    // Return the icon.
    return h_icon;
  }
}
pub fn getfontwh(w :HWND, font :HFONT) -> POINT
{
  unsafe
  {
    let dc = GetDC(w) as HDC;
    let mut char_w : INT = 0;
    let mut tm = TEXTMETRICW
    {
      tmHeight: 0, tmAscent: 0, tmDescent: 0, tmInternalLeading: 0, tmExternalLeading: 0, 
      tmAveCharWidth: 0, tmMaxCharWidth: 0, tmWeight: 0, tmOverhang: 0, tmDigitizedAspectX: 0, 
      tmDigitizedAspectY: 0, tmFirstChar: 0, tmLastChar: 0, tmDefaultChar: 0, 
      tmBreakChar: 0, tmItalic: 0, tmUnderlined: 0, tmStruckOut: 0, tmPitchAndFamily: 0, tmCharSet: 0
    };
    SelectObject(dc, font as HGDIOBJ ); 
    GetCharWidth32W(dc, 0 as UINT, 0 as UINT, &mut char_w); 
    GetTextMetricsW(dc, &mut tm);
    ReleaseDC(w, dc);
    POINT{x:char_w,y:tm.tmHeight}
  }
}
