//========================================================================
// read - simple win32 text editor written in rust
//------------------------------------------------------------------------
// Copyright (c) 2019 Ji Wong Park <sailfish009@gmail.com>
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would
//    be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and must not
//    be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source
//    distribution.
//
//========================================================================

fn copy(_w :HWND, _f :HFONT)
{
  unsafe
  {
    let region = {REGION.lock().unwrap()};
    if region.s != region.e
    {
      let mut buffer = String::new();
      setbuffer(region.s, region.e, &mut buffer);
      OpenClipboard(null()); EmptyClipboard();
      let hg :HGLOBAL = GlobalAlloc(LMEM_MOVEABLE, region.e - region.s);
      if hg == null() {CloseClipboard();return;}
      memcpy(buffer.as_ptr(), GlobalLock(hg) as *mut u8,  region.e - region.s);
      GlobalUnlock(hg);
      SetClipboardData(CF_TEXT, hg);
      CloseClipboard();
      GlobalFree(hg);
    }
  }
}

fn paste(_w :HWND, _f :HFONT)
{
  unsafe
  {
    OpenClipboard(null());
    let handle :HANDLE = GetClipboardData(CF_TEXT);
    let ptr = GlobalLock(handle) as *mut u8;
    let data = from_raw_parts(ptr, strlength(ptr));
    let buffer : String = String::from_utf8_lossy(data).to_string();
    GlobalUnlock(handle);
    CloseClipboard();
    draw(_w, _f, buffer); 
  }
}

fn file(_w :HWND, _f :HFONT, path :String, mode :u8)
{
  let result = File::open(path);
  match result
  {
    Ok(mut result) =>
    {
      match mode
      {
        // read default size from file
        0 =>
        {
          hidecaret(_w);
          clear();
          clearscreen(_w);
          *POS.lock().unwrap() = POINT{x:0, y:0};
          let mut buffer = [0; PAGE];
          let mut handle = result.take(PAGE as u64);
          let read_result = handle.read(&mut buffer);

          match read_result
          {
            Ok(_size) =>
            {
              draw_page(_w, _f, buffer);
              showcaret(_w);
              *END.lock().unwrap() = 1;
            },
            // error
            _ => {},
          }
        },
        // read default size from next block of file
        1 =>
        {
          hidecaret(_w);
          clear();
          clearscreen(_w);
          *POS.lock().unwrap() = POINT{x:0, y:0};
          // let index = {INDEX.lock().unwrap()};
          // TO DO: convert newline to byte offset
          // result.seek(SeekFrom::Start(nbyte));
          // let mut buffer = [0; PAGE];
          // let mut handle = result.take(PAGE as u64);
          // handle.read(&mut buffer);
          // draw_page(_w, _f, buffer);
          showcaret(_w);
          *END.lock().unwrap() = 1;
        },
        // read all from file 
        2 =>
        {
          hidecaret(_w);
          clear();
          clearscreen(_w);
          *POS.lock().unwrap() = POINT{x:0, y:0};
          let mut buffer = String::new();
          let read_result = result.read_to_string(&mut buffer);
          match read_result
          {
            Ok(_size) =>
            {
              draw(_w, _f, buffer);
              showcaret(_w);
              *END.lock().unwrap() = 1;
            },
            // error
            _ => {},
          }
        },
        // write
        _ =>
        {
        },
      }
    },
    // file open failed.
    _ => 
    {
      println!("file error");
    },
  }
}

