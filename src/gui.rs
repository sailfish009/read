//========================================================================
// read - simple win32 text editor written in rust
//------------------------------------------------------------------------
// Copyright (c) 2019 Ji Wong Park <sailfish009@gmail.com>
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would
//    be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and must not
//    be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source
//    distribution.
//
//========================================================================

fn wstr(str : &str) -> Vec<u16>{OsStr::new(str).encode_wide().chain(Some(0).into_iter()).collect()}
fn window(c :Vec<u16>, win :Vec<u16>, proc :WNDPROC, icon :HICON, style :DWORD, x :i32, y :i32, w :i32, h :i32, v :LPVOID) -> HWND
{
  unsafe
  {
    let wnd = WNDCLASSEXW
    {
      cbSize: mem::size_of::<WNDCLASSEXW>() as u32, style: 0, lpfnWndProc: proc, cbClsExtra: 0, cbWndExtra: 0,
      hInstance: 0 as HINSTANCE, hIcon: icon as HICON, hCursor: 0 as HICON, 
      hbrBackground: 0 as HBRUSH, lpszMenuName: 0 as LPCWSTR, lpszClassName: c.as_ptr(), hIconSm: icon as HICON,
    };
    RegisterClassExW(&wnd);
    CreateWindowExW(0, c.as_ptr(), win.as_ptr(), style, x, y, w, h, 0 as HWND, 0 as HMENU, 0 as HINSTANCE, v)
  }
}
// fn subwindow(c :Vec<u16>, win :Vec<u16>, proc :WNDPROC, icon :HICON, style :DWORD, x :i32, y :i32, w :i32, h :i32, v :LPVOID) -> HWND
// {
//   unsafe
//   {
//     let wnd = WNDCLASSEXW
//     {
//       cbSize: mem::size_of::<WNDCLASSEXW>() as u32, style: 0, lpfnWndProc: proc, cbClsExtra: 0, cbWndExtra: 0,
//       hInstance: 0 as HINSTANCE, hIcon: icon as HICON, hCursor: 0 as HICON, 
//       hbrBackground: 0 as HBRUSH, lpszMenuName: 0 as LPCWSTR, lpszClassName: c.as_ptr(), hIconSm: icon as HICON,
//     };
//     RegisterClassExW(&wnd);
//     CreateWindowExW(WS_EX_CLIENTEDGE, c.as_ptr(), win.as_ptr(), style, x, y, w, h, 0 as HWND, 0 as HMENU, 0 as HINSTANCE, v)
//   }
// }
// fn control(c :Vec<u16>, win :Vec<u16>, hwnd :HWND, id :HMENU, style :DWORD, x :i32, y :i32, w :i32, h :i32) -> HWND
// {
//   unsafe {CreateWindowExW(0, c.as_ptr(), win.as_ptr(), style, x, y, w, h, hwnd, id, GetWindowLongPtrW(hwnd, GWL_HINSTANCE) as HINSTANCE, 0 as LPVOID)}
// }
fn font(font_name :Vec<u16>, font_size :i32) -> HFONT
{
  unsafe
  {
    CreateFontW(font_size, 0, 0, 0, FW_LIGHT, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, font_name.as_ptr())
  }
}
fn change_state(w :HWND, icon :HICON)
{
  unsafe{SendMessageW(w, WM_SETICON, ICON_SMALL as usize, icon as LPARAM);}
}
fn clearscreen(w :HWND)
{
  // bug? call RedrawWindow() only should be work.
  // work around: call InvalidateRect() and call RedrawWindow()
  unsafe{InvalidateRect(w, null(), 1); RedrawWindow(w, null(), null(), RDW_INVALIDATE | RDW_UPDATENOW);}
}
fn clearch(w :HWND, i :usize, r :i32)
{
  let rect = getrect(i);
  unsafe {InvalidateRect(w, &rect, r);}
  // drawtext(w, f, &CH{x:getx(i),y:gety(i),c:' '}, 1, 0);
}
fn updaterange(_w :HWND, _f :HFONT, s :usize, e :usize, mode :u8)
{
  // mode 0: - , _: +
  let mut vec = {TEXT.lock().unwrap()};
  match mode 
  { 
    0=> {let x = -1; for i in s..=e {vec[i].x += x;}}, 
    1=> {let x = 1;  for i in s..=e {vec[i].x += x;}}, 
    2=> {let y = -1; for i in s..=e {vec[i].y += y;}}, 
    3=> {let y = 1;  for i in s..=e {vec[i].y += y;}}, 
    _=> {} 
  }
}
fn clearrange(w :HWND, f :HFONT, s :usize, e :usize)
{
  for i in s..=e {drawtext(w, f, &CH{x:getx(i),y:gety(i),c:' '}, 1, 0);}
}
fn drawrange(w :HWND, f :HFONT, s :usize, e :usize)
{
  for i in s..=e {drawtext(w, f, &CH{x:getx(i),y:gety(i),c:getc(i)}, 1, 0);}
}
fn getrect(i :usize) -> RECT
{
  let v = {TEXT.lock().unwrap()};
  let w = getw();
  let h = geth();
  RECT{left:v[i].x*w,top:(v[i].y*h),right:(v[i].x*w+w),bottom:(v[i].y*h+h)}
}
// fn decpos(x :i32, y: i32, lastx :i32)
// {
//   if x > 0 
//   {
//     POS.lock().unwrap().x -= 1;
//   }
//   else if y > 0 
//   {
//     POS.lock().unwrap().y -= 1;
//     POS.lock().unwrap().x = lastx;
//   }
// }
fn getpos(x :i32, y:i32) -> Option<POINT>
{
  let vec = {TEXT.lock().unwrap()}; let w = getw(); let h = geth();
  for v in vec.iter()
  {
    if (v.y*h <= y && v.y*h+h > y) && (v.x*w <= x && v.x*w+w > x) {return Some(POINT{x:v.x, y:v.y});}
  }
  None
}
fn setpos(x :i32, y :i32){*POS.lock().unwrap() = POINT{x:x, y:y};}
fn setposx(x :i32){POS.lock().unwrap().x = x;}            
fn setposy(y :i32){POS.lock().unwrap().y = y;}
fn setposmx(x :i32){POS.lock().unwrap().x -= x;}          fn setpospx(x :i32){POS.lock().unwrap().x += x;}
fn setposmy(y :i32){POS.lock().unwrap().y -= y;}          fn setpospy(y :i32){POS.lock().unwrap().y += y;}
fn getposx() -> i32 {POS.lock().unwrap().x}               fn getposy() -> i32 {POS.lock().unwrap().y}
fn getw() -> i32 {*CHX.lock().unwrap()}                   fn geth() -> i32 {*CHY.lock().unwrap()}
fn setmode(mode :u8){*MODE.lock().unwrap() = mode}        fn getmode() -> u8 {*MODE.lock().unwrap()}
fn setstate(state :u32, mode :u8)
{
  match mode {0 => {*STATE.lock().unwrap() |= state}, _ => {*STATE.lock().unwrap() &= state}}
}
fn getstate() -> u32 {*STATE.lock().unwrap()}
fn setregion(pos :usize, mode :u8)
{
  let start = {REGION.lock().unwrap().s};
  match mode
  {
    0 => {REGION.lock().unwrap().s = pos;}, 
    _ => 
    {
      match start > pos
      {
        true => {REGION.lock().unwrap().s = pos; REGION.lock().unwrap().e = start; }
        _ => {REGION.lock().unwrap().e = pos;},
      }
    }, 
  }
}
fn updatepos(x :i32, y: i32)
{
  let w = getw();
  let h = geth(); 
  let vec = {TEXT.lock().unwrap()};
  for v in vec.iter()
  {
    if (v.y*h <= y && v.y*h+h > y) && (v.x*w <= x && v.x*w+w > x) {setpos(v.x, v.y);break;}
  } 
}
fn setcaretpos(x :i32, y :i32){unsafe{SetCaretPos(x, y);}}
fn delete(w :HWND, f :HFONT, mode :u8)
{
  let x = getposx();
  match mode { 0=> (), _=> { if x == 0 {return;} setposx(x-1);} } 
  hidecaret(w);
  let mut length = getlength();
  let index = getindex();
  match index 
  {
    None => (),
    _ =>
    {
      let index = index.unwrap();
      for i in index..length { if getc(i) == NEWLINE {length = i; break;} }
      clearrange(w, f, index, length-1);
      remove(index);
      let length = getlength();
      match length
      {
        0 => (),
        _ =>
        {
          updaterange(w, f, index, length-1, 0);
          drawrange(w, f, index, length-1);
        },
      }
    },
  }
  showcaret(w);
}
fn enter(w :HWND, f :HFONT)
{
  let index = getindex();
  match index
  {
    None => 
    {
      save(CH{x:getposx(),y:getposy(),c:NEWLINE});
    }, 
    _ => 
    {  
      let i = index.unwrap();
      println!("index : {0}", i);
      match end(i)
      {
        // end of text
        true => 
        {
          *END.lock().unwrap() = 1;

          save(CH{x:getposx(),y:getposy(),c:NEWLINE});
        },
        // middle of text 
        _ => 
        {
          *END.lock().unwrap() = 0;

          let length = getlength();

          if getc(i) != NEWLINE
          {
            let i = getlastx();
            clearrange(w, f, i+1, length-1);
            insert(i, CH{x:getposx(),y:getposy(),c:NEWLINE});
            updaterange(w, f, i+1, length-1, 3);
            drawrange(w, f, i+1, length-1);
          }
          else
          {
            clearrange(w, f, i+1, length-1);
            insert(i, CH{x:getposx(),y:getposy(),c:NEWLINE});
            updaterange(w, f, i+1, length, 3);
            drawrange(w, f, i+1, length);
          }
        }
      }
    }
  }
}
fn draw_page(w :HWND, f :HFONT, buffer :[u8;PAGE])
{
  for c in buffer.iter()
  {
    let x = getposx();
    let y = getposy();
    match *c as char
    {
      NEWLINE => {save(CH{x:x, y:y,c:*c as char}); setposx(0); setpospy(1);},
      NOTLINE => {},
      _ => 
      { 
        let ch = CH{x:x,y:y,c:*c as char};
        let screen = {*SCREEN.lock().unwrap()};
        if y < screen {drawtext(w, f, &ch, 0, 0);}
        save(ch);
        setpospx(1);
      },
    }
  }
}
fn draw(w :HWND, f :HFONT, buffer :String)
{
  for c in buffer.chars()
  {
    let x = getposx();
    let y = getposy();
    match c
    {
      NEWLINE => {save(CH{x:x, y:y,c:c}); setposx(0); setpospy(1);},
      NOTLINE => {},
      _ => 
      { 
        let ch = CH{x:x,y:y,c:c};
        let screen = {*SCREEN.lock().unwrap()};
        if y < screen {drawtext(w, f, &ch, 0, 0);}
        save(ch);
        setpospx(1);
      },
    }
  }
}
fn copy2(w :HWND, f :HFONT, mode :u8)
{
  let mut buffer = {BUFFER.lock().unwrap()};
  let region = {REGION.lock().unwrap()};
  if region.s != region.e
  {
    setbuffer(region.s, region.e, &mut buffer);
    match mode { 0 =>(), _ => {redraw(w, f, 0, 1, region.s, region.e); }}
  }
}
fn redraw(w :HWND, f :HFONT, offset: isize, mode :u8, s :usize, e :usize)
{
  match mode 
  {
    1 =>
    {
      if s != e  // start != end
      {
        let vec = {TEXT.lock().unwrap()};
        vec[s..=e].iter().for_each(|ref x| drawtext(w, f, &x, 0, 0));
      }
    },
    _ => 
    {
      clearscreen(w);
      let vec = {TEXT.lock().unwrap()};
      vec.iter().for_each(|ref x| drawtext(w, f, &x, 2, offset));
    }
  }
}
fn drawtext(w :HWND, f :HFONT, c :&CH, p :WPARAM, l :LPARAM)
{
  unsafe
  {
    let dc = GetDC(w) as HDC;
    SelectObject(dc, f as HGDIOBJ);
    match CURRENT_WINDOW
    {
      // console
      1 =>
      {
        SetTextColor(dc, RGB(C_R_A,C_G_A,C_B_A));
      },
      // main window
      _ =>
      {
        SetTextColor(dc, RGB(R_A,G_A,B_A));
      }
    }
    // SetTextColor(dc, RGB(R_A,G_A,B_A));
    
    let string :String = c.c.to_string();
    let ch = wstr(&string); let ch_w = getw(); let ch_h = geth(); 

    match p 
    {
      2 => 
      {
        let y = c.y + l as i32;
        SetBkColor(dc, RGB(R_B,G_B,B_B));
        TextOutW(dc, c.x * ch_w, y * ch_h, ch.as_ptr(), 1);
      },
      // 0: display and save, 1: no save
      _ => 
      {
        match l
        {
          1 => {SetBkColor(dc, RGB(R_C,G_C,B_C));},
          _ => {SetBkColor(dc, RGB(R_B,G_B,B_B));},
        }
        TextOutW(dc, c.x * ch_w, c.y * ch_h, ch.as_ptr(), 1);
      },
    }
    ReleaseDC(w, dc);
  }
}
fn showcaret(w :HWND)
{
  unsafe
  {
    let x = getposx(); let y = getposy(); let w_ = getw(); let h_ = geth(); 
    let index = getindex();
    match index
    {
      None => 
      {
        match getlength()
        {
          0 => {setcaretpos(0,0); ShowCaret(w);},
          _ => {setcaretpos(x*w_, y*h_); ShowCaret(w);}
        }
      },
      _ =>
      {
        let i = index.unwrap(); setcaretpos(getx(i)*w_, gety(i)*h_); ShowCaret(w);
      },
    }
  }
}
fn hidecaret(w :HWND) {unsafe{HideCaret(w);}}
fn background(hwnd :HWND, brush :&HBRUSH)
{
  unsafe
  {
    #[cfg(target_pointer_width = "32")]{SetClassLongPtrW(hwnd, GCLP_HBRBACKGROUND, *brush as i32);}   // 32bit
    #[cfg(target_pointer_width = "64")]{SetClassLongPtrW(hwnd, GCLP_HBRBACKGROUND, *brush as isize);} // 64bit
  }
}
