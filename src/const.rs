//========================================================================
// read - simple win32 text editor written in rust
//------------------------------------------------------------------------
// Copyright (c) 2019 Ji Wong Park <sailfish009@gmail.com>
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would
//    be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and must not
//    be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source
//    distribution.
//
//========================================================================

#[macro_use]
extern crate lazy_static;
extern crate winapi; 

use winapi::shared::windowsx::{GET_X_LPARAM, GET_Y_LPARAM};
use winapi::um::{shellapi as shell, winuser as user, wingdi as gdi, winbase as win, minwinbase::LMEM_MOVEABLE, winnt::{LPCWSTR, LONG, HANDLE}};
use winapi::um::{dwmapi as dwm};
// use winapi::shared::windef::{HWND, HMENU, HBRUSH, HICON, HFONT, HGDIOBJ, HBITMAP, HDC, RECT, POINT, LPRECT};
use winapi::shared::windef::{HWND, HMENU, HBRUSH, HICON, HFONT, HGDIOBJ, HBITMAP, HDC, RECT, POINT};
use winapi::shared::minwindef::{UINT, DWORD, MAX_PATH, HINSTANCE, HGLOBAL, LRESULT, LPVOID, WPARAM, LPARAM, HIWORD};
use std::{ptr, mem, io::Read, fs::File, ffi::OsStr, sync::Mutex, string::String, os::windows::ffi::OsStrExt};
// use user::{WS_OVERLAPPEDWINDOW, WS_VISIBLE, WS_CHILD, WS_CLIPCHILDREN, WS_BORDER, WS_POPUP, WNDCLASSEXW, WNDPROC, WS_EX_CLIENTEDGE};
// use user::{WS_OVERLAPPEDWINDOW, WS_VISIBLE, WS_BORDER, WS_POPUP, WNDCLASSEXW, WNDPROC, WS_EX_CLIENTEDGE};
use user::{WS_OVERLAPPEDWINDOW, WS_VISIBLE, WS_BORDER, WS_POPUP, WNDCLASSEXW, WNDPROC};
// use user::{WS_OVERLAPPED, WS_CAPTION, WS_THICKFRAME};
use user::{CF_TEXT, OpenClipboard, EmptyClipboard, SetClipboardData, GetClipboardData, CloseClipboard};
use user::{RegisterClassExW, CreateWindowExW, ICON_SMALL, InvalidateRect, RedrawWindow};
use user::{RDW_INVALIDATE, RDW_UPDATENOW, SetCaretPos, GetDC, ReleaseDC, ShowWindow, ShowCaret, HideCaret};
// use user::{SetFocus, SW_SHOW, SW_HIDE, SetWindowLongPtrW, GetWindowLongPtrW, GWLP_USERDATA, GWL_HINSTANCE};
use user::{SetFocus, SW_SHOW, SW_HIDE, SetWindowLongPtrW, GetWindowLongPtrW, GWLP_USERDATA};
use user::{GetAsyncKeyState, VK_CONTROL, SetCursor, LoadCursorW, IDC_ARROW, GET_WHEEL_DELTA_WPARAM};
use user::{SC_MINIMIZE, CreateCaret, GCLP_HBRBACKGROUND, MoveWindow, SetParent, MSG}; 
// use user::{DestroyWindow, SendMessageW, PostQuitMessage, DefWindowProcW, DefFrameProcW, GetMessageW, SetClassLongPtrW}; 
use user::{DestroyWindow, SendMessageW, PostQuitMessage, DefWindowProcW, GetMessageW, SetClassLongPtrW}; 
use user::{WM_SETICON, WM_CREATE, WM_CHAR, WM_DROPFILES, WM_LBUTTONDOWN, WM_LBUTTONUP, WM_MOUSEMOVE, WM_MOUSEWHEEL}; 
use user::{WM_PAINT, WM_SIZE, WM_SETFONT, WM_SYSCOMMAND, WM_DESTROY, WM_CLOSE, WM_QUIT}; 
// use user::{LPCREATESTRUCTW, SS_CENTER, TranslateMessage, DispatchMessageW};
use user::{LPCREATESTRUCTW, TranslateMessage, DispatchMessageW};
// use user::{GetParent, GetWindowRect, SetWindowPos, SWP_NOMOVE, SWP_NOZORDER, WS_HSCROLL, WS_VSCROLL};
use user::{GetParent, GetWindowRect, SetWindowPos, SWP_NOZORDER, WS_HSCROLL, WS_VSCROLL};
// use dwm::{DwmSetWindowAttribute, DwmExtendFrameIntoClientArea};
use dwm::{DwmSetWindowAttribute};
use gdi::{SelectObject, DeleteObject, SetTextColor, RGB, SetBkColor, TextOutW};
use gdi::{CreateSolidBrush, DEFAULT_PITCH, CreateFontW, FW_LIGHT, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY};
use shell::{HDROP, DragQueryFileW, DragFinish, DragAcceptFiles};
use win::{GlobalAlloc, GlobalLock, GlobalUnlock, GlobalFree};
use ptr::{copy_nonoverlapping as memcpy, null_mut as null};
use winapi::ctypes::c_void;
use std::slice::from_raw_parts;

// unused
// use winapi::shared::windef::{COLORREF, HPEN, LPPOINT};
// use winapi::shared::minwindef::{INT, LOWORD};
// use std::{io::{Read, Write}};
// use user::{WS_SYSMENU, WS_CAPTION, CreatePopupMenu, FillRect, BM_SETIMAGE, IMAGE_BITMAP, WM_DRAWITEM}; 
// use user::{BS_PUSHBUTTON, BS_DEFPUSHBUTTON, BS_BITMAP, BS_FLAT, BS_OWNERDRAW, GWLP_WNDPROC};
// use user::{LPDRAWITEMSTRUCT, AdjustWindowRect};
// use gdi::{TEXTMETRICW, GetCharWidth32W, GetTextMetricsW, CreateCompatibleDC, CreateCompatibleBitmap, SetDCBrushColor};
// use gdi::{GetStockObject, DC_BRUSH, DeleteDC, Rectangle, SetBkMode, TRANSPARENT}; 

mod utils;
use utils::{geticon, getfontwh};

const PAGE: usize = 2816; // 88x32
const SX: i32 = 200; const SY: i32 = 200; const W:  i32 = 800; const H:  i32 = 600;
// main window text color
// const R_A: u8 = 250; const G_A: u8 = 250; const B_A: u8 = 250;
const R_A: u8 = 0; const G_A: u8 = 0; const B_A: u8 = 0;
// main window background color
// const R_B: u8 = 0;   const G_B: u8 = 0;   const B_B: u8 = 0;
const R_B: u8 = 57;   const G_B: u8 = 190;   const B_B: u8 = 239;
const R_C: u8 = 100; const G_C: u8 = 100; const B_C: u8 = 100;
const NEWLINE :char = '\n'; const NOTLINE :char = '\r';
// const MEMU_OPEN: i32 = 1001;
// const MEMU_SAVE: i32 = 1002;
// const MEMU_RUN_: i32 = 1003;
// const MEMU_EXIT: i32 = 1004;

// console window background color
// const C_R_A: u8 = 250; const C_G_A: u8 = 250; const C_B_A: u8 = 250;
// const C_R_B: u8 = 0;   const C_G_B: u8 = 0;   const C_B_B: u8 = 0;

const C_R_A: u8 = 0; const C_G_A: u8 = 0; const C_B_A: u8 = 0;
const C_R_B: u8 = 250;   const C_G_B: u8 = 250;   const C_B_B: u8 = 250;

// main window: 0, console: 1
static mut CURRENT_WINDOW : u32 = 0;
static mut MAIN_X : i32 = 0;
static mut MAIN_Y : i32 = 0;
