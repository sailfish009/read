//========================================================================
// read - simple win32 text editor written in rust
//------------------------------------------------------------------------
// Copyright (c) 2019 Ji Wong Park <sailfish009@gmail.com>
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would
//    be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and must not
//    be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source
//    distribution.
//
//========================================================================

pub unsafe extern "system" fn window_proc(w :HWND, msg :UINT, p :WPARAM, l :LPARAM) -> LRESULT
{
  match msg 
  {
    WM_CREATE => 
    {
      let param = &*(l as LPCREATESTRUCTW);
      #[cfg(target_pointer_width = "32")] {SetWindowLongPtrW(w, GWLP_USERDATA,  param.lpCreateParams as i32);}    // 32bit
      #[cfg(target_pointer_width = "64")] {SetWindowLongPtrW(w, GWLP_USERDATA,  param.lpCreateParams as isize);}  // 64bit
    },

    // WM_CHAR => 
    // {
    //   let wm_ptr = GetWindowLongPtrW(w, GWLP_USERDATA) as LPVOID;
    //   let wm :&mut WM =  &mut *(wm_ptr as *mut WM);
    //   match GetAsyncKeyState(VK_CONTROL) as u16 & 0x8000 {0 => edit(w, p, wm), _ => ctrl(w, p, wm),}
    // },
    // WM_RBUTTONDOWN => 
    // {
    //   let menu = CreatePopupMenu(); 
    // },

    WM_DROPFILES => 
    {
      let hdrop = p as HDROP;
      let _file = DragQueryFileW(hdrop, 0xFFFFFFFF, null(), 0);

      if _file != 1
      {
        println!("multiple files not supported");
        DragFinish(hdrop);
      }
      else
      {
        let mut v = vec![0u16; MAX_PATH as usize];
        DragQueryFileW(hdrop, 0, v.as_mut_ptr(), MAX_PATH as u32);
        DragFinish(hdrop);

        let mut path = String::new();
        for val in v.iter() 
        {
          let c = (*val & 0xFF) as u8;
          if c == 0 {break;} else { path.push(c as char);}
        } 
        let hwnd = GetParent(w);
        // let wm_ptr = GetWindowLongPtrW(w, GWLP_USERDATA) as LPVOID;
        let wm_ptr = GetWindowLongPtrW(hwnd, GWLP_USERDATA) as LPVOID;
        let wm :&mut WM =  &mut *(wm_ptr as *mut WM);
        let font = wm.font;
        file(w, font, path, 0); 
      }
    },

    WM_LBUTTONDOWN => 
    {
      if CURRENT_WINDOW != 0
      {
        CURRENT_WINDOW = 0;
        setposx(MAIN_X);
        setposy(MAIN_Y);
        CreateCaret(w, 0 as HBITMAP, 1, geth());
      }

      SetFocus(w);
      ShowCaret(w);
      hidecaret(w); updatepos(GET_X_LPARAM(l), GET_Y_LPARAM(l)); showcaret(w);
      match getmode() & 0x10
      {
        0x10 => {},
        _ =>
        {
          match getstate() & 0x40
          {
            0x40 =>
            {
              setstate(0xFFFFFFBF, 1);
              let region = {REGION.lock().unwrap()};
              let hwnd = GetParent(w);
              let wm_ptr = GetWindowLongPtrW(hwnd, GWLP_USERDATA) as LPVOID;
              let wm :&mut WM =  &mut *(wm_ptr as *mut WM);
              redraw(w, wm.font, 0, 1, region.s, region.e);
            },
            _ => (),
          }
          setstate(0x80, 0);
          let index = getindex();
          match index { None => (), _ => {setregion(index.unwrap(), 0);}}
        }
      }
    },

    WM_SIZE => 
    {
      let wm_ptr = GetWindowLongPtrW(w, GWLP_USERDATA) as LPVOID;
      let wm :&mut WM =  &mut *(wm_ptr as *mut WM);
      // setstate(0x20);
      // println!("w: {0}, h: {1}", LOWORD(l as u32), HIWORD(l as u32));
      let w_h = HIWORD(l as u32);
      let ch_h = {*CHY.lock().unwrap()} as u16;
      if ch_h != 0 {*SCREEN.lock().unwrap() = (w_h / ch_h) as i32 ;}

      let mut m_r = RECT{left :0, top :0, right :0, bottom :0};
      GetWindowRect(w, &mut m_r);
      let m_w = m_r.right - m_r.left;
      let m_h = m_r.bottom - m_r.top;
      let top = wm.sw[2];
      let bottom = wm.sw[1];
      SetWindowPos(bottom, w, 0, 2*m_h/3, m_w, m_h/3, SWP_NOZORDER);
      SetWindowPos(top, w, 0, 0, m_w, 2*m_h/3, SWP_NOZORDER);
    },
    WM_SYSCOMMAND => {match p & 0xFFF0 {SC_MINIMIZE => {setstate(0x10, 0);}, _ => ()}},
    WM_CLOSE => {DestroyWindow(w);},
    WM_DESTROY => {DragAcceptFiles(w, 0); PostQuitMessage(0);},
    _ => (),
  }
  DefWindowProcW( w, msg, p, l)
  // DefFrameProcW( w, msg, p, l)
}
pub unsafe extern "system" fn sw1_proc(w :HWND, msg :UINT, p :WPARAM, l :LPARAM) -> LRESULT
{
  match msg 
  {
    WM_LBUTTONDOWN => {println!("win1 button clicked!");},
    WM_CLOSE => {DestroyWindow(w);}, WM_DESTROY => {PostQuitMessage(0);}, _ => (),
  }
  DefWindowProcW( w, msg, p, l)
}
// bottom
pub unsafe extern "system" fn sw2_proc(w :HWND, msg :UINT, p :WPARAM, l :LPARAM) -> LRESULT
{
  match msg 
  {
    WM_LBUTTONDOWN => 
    { 
        // let hwnd = GetParent(w);
        // hidecaret(hwnd);

        if CURRENT_WINDOW == 0
        {
          CURRENT_WINDOW = 1;
          MAIN_X = getposx();
          MAIN_Y = getposy();
          CreateCaret(w, 0 as HBITMAP, 1, geth());
        }

        SetFocus(w);
        ShowCaret(w);
        // hidecaret(w); updatepos(GET_X_LPARAM(l), GET_Y_LPARAM(l)); showcaret(w);
        hidecaret(w); setpos(0, 0); showcaret(w);

        println!("win2 button clicked!");
    },
    WM_CLOSE => {DestroyWindow(w);}, WM_DESTROY => {PostQuitMessage(0);}, _ => (),
  }
  DefWindowProcW( w, msg, p, l)
}
// top
pub unsafe extern "system" fn sw3_proc(w :HWND, msg :UINT, p :WPARAM, l :LPARAM) -> LRESULT
{
  match msg 
  {
    WM_CREATE => 
    {
      let param = &*(l as LPCREATESTRUCTW);
      #[cfg(target_pointer_width = "32")] {SetWindowLongPtrW(w, GWLP_USERDATA,  param.lpCreateParams as i32);}    // 32bit
      #[cfg(target_pointer_width = "64")] {SetWindowLongPtrW(w, GWLP_USERDATA,  param.lpCreateParams as isize);}  // 64bit
    },

    WM_CHAR => 
    {
      // let hwnd = GetParent(w);
      let wm_ptr = GetWindowLongPtrW(w, GWLP_USERDATA) as LPVOID;
      let wm :&mut WM =  &mut *(wm_ptr as *mut WM);
      // let top = wm.sw[2];
      // match GetAsyncKeyState(VK_CONTROL) as u16 & 0x8000 {0 => edit(top, p, wm), _ => ctrl(top, p, wm),}
      match GetAsyncKeyState(VK_CONTROL) as u16 & 0x8000 {0 => edit(w, p, wm), _ => ctrl(w, p, wm),}
    },

    WM_LBUTTONUP => 
    {
      match getmode() & 0x10
      {
        0x10 => {},
        _ =>
        {
          hidecaret(w); updatepos(GET_X_LPARAM(l), GET_Y_LPARAM(l)); showcaret(w);
          let index = getindex();
          match index {None => (), _ => setregion(index.unwrap(), 1)}
          setstate(0xFFFFFF7F, 1);
          let region = {REGION.lock().unwrap()};
          match region.s != region.e {true => setstate(0x40, 0), _ => (),}
        }
      }
    },

    WM_MOUSEMOVE => 
    {
      SetCursor(LoadCursorW(null(), IDC_ARROW));
      match getstate() & 0x80
      {
        0x80 =>
        {
          let pos = getpos(GET_X_LPARAM(l), GET_Y_LPARAM(l));
          match pos
          {
            None => {},
            _ =>
            {
              let p = pos.unwrap();
              let hwnd = GetParent(w);
              // let wm_ptr = GetWindowLongPtrW(w, GWLP_USERDATA) as LPVOID;
              let wm_ptr = GetWindowLongPtrW(hwnd, GWLP_USERDATA) as LPVOID;
              let wm :&mut WM =  &mut *(wm_ptr as *mut WM);
              let font = wm.font;
              let c = getchar(p.x, p.y);
              match c
              {
                None => (), 
                _ => 
                {
                  let c = c.unwrap();
                  drawtext(w, font, &CH{x:p.x, y:p.y, c:c}, 1, 1);
                  let mut rbuffer = {RBUFFER.lock().unwrap()};
                  rbuffer.push(c);
                },
              }
            },
          }
        },
        _=> (),
      }
    },
    WM_MOUSEWHEEL => 
    {
      let delta = GET_WHEEL_DELTA_WPARAM(p);
      let hwnd = GetParent(w);
      let wm_ptr = GetWindowLongPtrW(hwnd, GWLP_USERDATA) as LPVOID;
      let wm :&mut WM =  &mut *(wm_ptr as *mut WM);
      let font = wm.font;
      match delta // let y = GET_Y_LPARAM(l);
      {
        120 =>  {wheel_up(w, font);},
        -120 => {wheel_down(w, font);},
        _ => {},
      }
    },

    WM_PAINT => 
    {
      let state = getstate();
      match state & 0x10
      {
        0x10 =>
        {
          setstate(0xFFFFFFEF, 1);
          let hwnd = GetParent(w);
          let wm_ptr = GetWindowLongPtrW(hwnd, GWLP_USERDATA) as LPVOID;
          let wm :&mut WM =  &mut *(wm_ptr as *mut WM);
          let font = wm.font;
          let top = wm.sw[2];
          let vec = {TEXT.lock().unwrap()};
          for v in vec.iter() {drawtext(top, font, &CH{x:v.x,y:v.y,c:v.c}, 1, 0);}
          // for v in vec.iter() {drawtext(w, font, &CH{x:v.x,y:v.y,c:v.c}, 1, 0);}
        },
        _ => {},
      }
    },


    _ => (),
    // WM_LBUTTONDOWN => {println!("win3 button clicked!");},
    // WM_CLOSE => {DestroyWindow(w);}, WM_DESTROY => {PostQuitMessage(0);}, _ => (),
  }
  DefWindowProcW( w, msg, p, l)
}
pub unsafe extern "system" fn sw4_proc(w :HWND, msg :UINT, p :WPARAM, l :LPARAM) -> LRESULT
{
  match msg 
  {
    WM_LBUTTONDOWN => {println!("win4 button clicked!");},
    WM_CLOSE => {DestroyWindow(w);}, WM_DESTROY => {PostQuitMessage(0);}, _ => (),
  }
  DefWindowProcW( w, msg, p, l)
}

